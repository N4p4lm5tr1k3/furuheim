function StatusChart(element,options){
  var self;
  var Element;
  var Options = {
    title: 'Aksjon 2020',
    value: 0,
    threshold: 20
  };

  self = Object.defineProperties({},{
    'element': {
      get: function(){ return Element; },
      set: function(value){
        if(typeof value == 'string'){
          Element = document.querySelector(value);
        }
        else if(value instanceof HTMLElement){
          Element = value;
        }
      }
    },
    'value': {
      get: function(){ return Options.value; },
      set: function(value){
        if(typeof value == 'number') Options.value = value;
      }
    },
    'threshold': {
      get: function(){ return Options.threshold; },
      set: function(value){
        if(typeof value == 'number') Options.threshold = value;
      }
    },
    'title': {
      get: function(){
        return Options.title;
      },
      set: function(value){
        Options.title = value;
      }
    }
  });

  self.element = element;

  Object.keys(options).forEach(function(key){
    if(key in Options) Options[key] = options[key];
  });

  function build(){
    Element.classList.add('widget','aksjon-status');
    Element.innerHTML = '<h3>' + Options.title + '</h3>';
    var inner = document.createElement('div');

    var data = {
      labels: [''],
      series: [
        [Options.value]
      ]
    };

    new Chartist.Bar(inner,data,{
      axisY: {
        low: 0,
        high: Options.value + Options.threshold
      },
      chartPadding: {
        left: 20,
        bottom: 0
      },
      plugins: [
    Chartist.plugins.ctAxisTitle({
      axisX: {
        axisTitle: 'Totalt: ' + (Options.value * 1000000).toLocaleString(),
        axisClass: 'ct-axis-title',
        offset: {
          x: 0,
          y: 30
        },
        textAnchor: 'middle'
      },
      axisY: {
        axisTitle: 'Millioner',
        axisClass: 'ct-axis-title',
        offset: {
          x: 0,
          y: 0
        },
        textAnchor: 'middle',
        flipTitle: false
      }
    })
  ]
    });

    Element.appendChild(inner);
  }

  build();

  return self;
};
